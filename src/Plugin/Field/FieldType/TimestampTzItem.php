<?php declare(strict_types = 1);

namespace Drupal\timestamp_tz\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Field\Plugin\Field\FieldType\TimestampItem;

/**
 * Defines the 'timestamp_tz' field type.
 *
 * @FieldType(
 *   id = "timestamp_tz",
 *   label = @Translation("Timestamp w/timezone"),
 *   description = {
 *     @Translation("Timestamp field, with an additional timezone column."),
 *     @Translation("Allows date/time to be set and displayed with a specific timezone."),
 *   },
 *   category = "date_time",
 *   default_widget = "timestamp_tz",
 *   default_formatter = "timestamp_tz",
 * )
 */
class TimestampTzItem extends TimestampItem {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $schema = parent::schema($field_definition);
    $schema['columns']['tz'] = [
      'type' => 'varchar',
      'length' => 50,
    ];
    return $schema;
  }

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition) {
    $values = parent::generateSampleValue($field_definition);
    $values['tz'] = 'America/New_York';
    return $values;
  }

}
