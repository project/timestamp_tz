<?php

namespace Drupal\timestamp_tz\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\TimestampFormatter;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'Timestamp w/tz' formatter.
 *
 * @FieldFormatter(
 *   id = "timestamp_tz",
 *   label = @Translation("Timestamp w/timezone"),
 *   field_types = {"timestamp_tz"},
 * )
 */
class TimestampTzFormatter extends TimestampFormatter {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
        'date_format' => 'medium',
        'custom_date_format' => '',
      ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);
    // Remove stuff that's not pertinent.
    $keys = array_flip(['date_format', 'custom_date_format']);
    return array_intersect_key($form, $keys);
  }

    /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    $date_format = $this->getSetting('date_format');
    $custom_date_format = $this->getSetting('custom_date_format');

    foreach ($items as $delta => $item) {
      $elements[$delta] = [
        '#theme' => 'time',
        '#attributes' => [
          'datetime' => $this->dateFormatter->format($item->value, static::CUSTOM_DATE_FORMAT, \DateTimeInterface::RFC3339, $item->tz),
        ],
        '#text' => $this->dateFormatter->format($item->value, $date_format, $custom_date_format, $item->tz, $langcode) . " ($item->tz)",
      ];
    }

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $date_format = $this->getSetting('date_format');
    $date_format = $date_format === static::CUSTOM_DATE_FORMAT ? $this->getSetting('custom_date_format') : $date_format;
    $summary[] = $this->t('Date format: @date_format', ['@date_format' => $date_format]);
    return $summary;
  }

}
