<?php

namespace Drupal\timestamp_tz\Plugin\Field\FieldWidget;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Datetime\Plugin\Field\FieldWidget\TimestampDatetimeWidget;
use Drupal\Core\Datetime\TimeZoneFormHelper;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines the 'timestamp_tz' field widget.
 *
 * @FieldWidget(
 *   id = "timestamp_tz",
 *   label = @Translation("Timestamp w/timezone"),
 *   field_types = {"timestamp_tz"},
 * )
 */
class TimestampTzWidget extends TimestampDatetimeWidget {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state): array {
    $default_value = NULL;
    $timezone = $items[$delta]->tz ?? date_default_timezone_get();
    if (isset($items[$delta]->value)) {
      $default_value = DrupalDateTime::createFromTimestamp($items[$delta]->value, new \DateTimeZone($timezone));
    }

    $element['value'] = $element + [
      '#type' => 'datetime',
      '#default_value' => $default_value,
      '#date_year_range' => '1902:2037',
      '#date_timezone' => $timezone,
    ];

    $element['tz'] = [
      '#type' => 'select',
      '#options' => TimeZoneFormHelper::getOptionsListByRegion(!$element['value']['#required']),
      '#default_value' => $timezone,
      '#required' => $element['value']['#required'],
    ];

    $element['tz_initial'] = [
      '#type' => 'value',
      '#value' => $timezone,
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    foreach ($values as &$item) {
      if (isset($item['value']) && $item['value'] instanceof DrupalDateTime) {
        $date = $item['value'];
      }
      elseif (isset($item['value']['object']) && $item['value']['object'] instanceof DrupalDateTime) {
        $date = $item['value']['object'];
      }
      else {
        $date = NULL;
      }

      $item['value'] = NULL;

      // Shift timestamp if the timezone has changed.
      if ($date !== NULL && $item['tz'] != $item['tz_initial']) {
        $date = $date->format('Y-m-d H:i:s');
        $date = DrupalDateTime::createFromFormat('Y-m-d H:i:s', $date, $item['tz']);
      }

      if ($date !== NULL) {
        $item['value'] = $date->getTimestamp();
      }
    }

    return $values;
  }

}
